FROM daocloud.io/python:3-onbuild
ADD . /code
WORKDIR /code
RUN pip3 install -r requirements.txt
CMD [ "python", "./run.py" ]
