from flask import Flask , g
from contextlib import closing
import sqlite3
from .views import init_views
from .data import con_db,init_db

DATABASE = './data.db'
SECRET_KEY = 'uncle-lu is a nice girl'
USERNAME = 'root'
PASSWORD = 'admin_password'

def create_app():
    app = Flask(__name__)
    init_views(app)
    app.config.from_object(__name__)
    app.config.from_envvar('FLASKR_SETTINGS', silent=True)
    con_db(app)
    #init_db(app)
    return app
